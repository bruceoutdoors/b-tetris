﻿''' notes:
''' - plays multiple audio files by generating random aliases
''' - pause/resume does not work for midi files
''' - can't set volume for midi files
''' - if looping wav, can't pause/resume. Only stop and play.
''' 

Public Class Audio
    Private Declare Function mciSendString Lib "winmm.dll" Alias "mciSendStringA" (ByVal lpszCommand As String, ByVal lpszReturnString As String, ByVal cchReturn As Integer, ByVal hwndCallback As Integer) As Integer

    Private name As String
    Private loop_timer As New System.Windows.Forms.Timer
    Private Filename As String

    Public Sub New(ByVal location As String)
        Me.Filename = location
        name = RndJumble(16)

        ' open file
        If Filename = "" Or Filename.Length <= 4 Then Exit Sub
        Dim fileFormat As String = Right(Filename, 3).ToLower
        If fileFormat = "mp3" Or fileFormat = "wav" Or fileFormat = "mid" Then
            mciSendString("open """ & Filename & """ alias " & name, "", 0, 0)
        Else
            Throw New Exception("File type not supported.")
        End If

        AddHandler loop_timer.Tick, AddressOf whenFinPlay
    End Sub

    Public Sub Play()
        mciSendString("play " & name & " from 0", "", 0, 0)
    End Sub

    Public Sub [Loop]()
        ' proper looping in only supported in mp3. For others we use a timer
        If Right(Filename, 3).ToLower = "mp3" Then
            mciSendString("play " & name & " repeat", "", 0, 0)
        Else
            loop_timer.Interval = 50
            loop_timer.Start()
        End If
    End Sub

    Public Sub Pause()
        mciSendString("pause " & name, "", 0, 0)
    End Sub

    Public Sub [Resume]()
        mciSendString("resume " & name, "", 0, 0)
    End Sub

    Public Sub [Stop]()
        mciSendString("stop " & name, "", 0, 0)
        loop_timer.Stop()
    End Sub

    Public Sub Close()
        mciSendString("close " & name, "", 0, 0)
    End Sub

    Public Sub setVolume(ByVal volume As Integer)
        mciSendString("setaudio " & name & " volume to " & volume, "", 0, 0)
    End Sub

    Private Sub whenFinPlay()
        Play()
        loop_timer.Interval = getLength
    End Sub

    ReadOnly Property getLength() As Integer
        Get
            Dim buf As String = Space(255)
            mciSendString("status " & name & " length", buf, 255, 0)
            buf = Replace(buf, Chr(0), "")
            If buf = "" Then
                Return 0
            Else
                ' midi files need to be tweaked so it can be somewhat in miliseconds
                If Right(Filename, 3).ToLower = "mid" Then
                    Return CInt(buf) * 108
                End If
                Return CInt(buf)
            End If
        End Get
    End Property

    ' produces a random jumble of characters so to ensure no one alias is the same
    Private Function RndJumble(ByVal length As Integer) As String
        Dim numOrChar As Boolean
        Dim thisString As String = ""
        For i = 0 To length
            Randomize()
            numOrChar = Int((2 * Rnd()))
            If numOrChar Then
                thisString += Chr(Math.Abs(Int(97 - 122) * Rnd() - 97))
            Else
                thisString += CStr(CInt((9 * Rnd()) + 1))
            End If
        Next
        Return thisString
    End Function
End Class
