﻿' class that contains all the shapes variant's properties
Public Class Shapes
    Public Structure blockData
        Public blockColor As Color
        ' Relative coordinates that define the shape
        Public Shape As List(Of Integer())
    End Structure

    ' all the different block shapes
    Public Shared L, I, J, O, S, T, Z As blockData

    Private Shared rnd As New Random

    Shared Sub New()
        With L
            .blockColor = Color.Aqua
            ' a list, terribly, needs to be instantiated before it can be used.
            ' bear this in mind when using lists
            .Shape = New List(Of Integer())
            .Shape.Add({0, 1, 0, 0, 0, -1, 1, -1})
            .Shape.Add({1, 0, 0, 0, -1, 0, -1, -1})
            .Shape.Add({0, -1, 0, 0, 0, 1, -1, 1})
            .Shape.Add({-1, 0, 0, 0, 1, 0, 1, 1})
        End With

        With I
            .blockColor = Color.Olive
            .Shape = New List(Of Integer())
            .Shape.Add({0, -1, 0, 0, 0, 1, 0, 2})
            .Shape.Add({-1, 0, 0, 0, 1, 0, 2, 0})
        End With

        With J
            .blockColor = Color.Brown
            .Shape = New List(Of Integer())
            .Shape.Add({0, -1, 0, 0, 0, 1, 1, 1})
            .Shape.Add({-1, 0, 0, 0, 1, 0, 1, -1})
            .Shape.Add({0, 1, 0, 0, 0, -1, -1, -1})
            .Shape.Add({1, 0, 0, 0, -1, 0, -1, 1})
        End With

        With O
            .blockColor = Color.DarkBlue
            .Shape = New List(Of Integer())
            .Shape.Add({1, 0, 0, 0, 0, -1, 1, -1})
        End With

        With S
            .blockColor = Color.OrangeRed
            .Shape = New List(Of Integer())
            .Shape.Add({0, 1, 0, 0, 1, 0, 1, -1})
            .Shape.Add({1, 0, 0, 0, 0, -1, -1, -1})
        End With

        With T
            .blockColor = Color.Gold
            .Shape = New List(Of Integer())
            .Shape.Add({0, -1, 0, 0, 0, 1, 1, 0})
            .Shape.Add({-1, 0, 0, 0, 1, 0, 0, -1})
            .Shape.Add({0, 1, 0, 0, 0, -1, -1, 0})
            .Shape.Add({1, 0, 0, 0, -1, 0, 0, 1})
        End With

        With Z
            .blockColor = Color.DarkMagenta
            .Shape = New List(Of Integer())
            .Shape.Add({0, -1, 0, 0, 1, 0, 1, 1})
            .Shape.Add({-1, 0, 0, 0, 0, -1, 1, -1})
        End With

    End Sub

    ' returns a random shape
    Public Shared Function genRand()
        ' gets a random number from 0 -> 6
        Dim randomINt As Integer = rnd.Next(7)
        Select Case randomINt
            Case 0
                Return I
            Case 1
                Return J
            Case 2
                Return O
            Case 3
                Return S
            Case 4
                Return T
            Case 5
                Return Z
            Case Else
                Return L
        End Select
    End Function

End Class
