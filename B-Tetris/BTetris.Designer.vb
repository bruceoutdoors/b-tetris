﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BTetris
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape2 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.RectangleShape1 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.GamePanel = New System.Windows.Forms.Panel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.highscore_lbl = New System.Windows.Forms.Label()
        Me.score_lbl = New System.Windows.Forms.Label()
        Me.PreviewPanel = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lines_lbl = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.level_lbl = New System.Windows.Forms.Label()
        Me.PressToStart_lbl = New System.Windows.Forms.Label()
        Me.FlashStart = New System.Windows.Forms.Timer(Me.components)
        Me.BlockTimer = New System.Windows.Forms.Timer(Me.components)
        Me.Leveler = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape2, Me.RectangleShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(415, 402)
        Me.ShapeContainer1.TabIndex = 0
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape2
        '
        Me.RectangleShape2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.RectangleShape2.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape2.FillGradientColor = System.Drawing.Color.Maroon
        Me.RectangleShape2.FillGradientStyle = Microsoft.VisualBasic.PowerPacks.FillGradientStyle.ForwardDiagonal
        Me.RectangleShape2.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.DiagonalCross
        Me.RectangleShape2.Location = New System.Drawing.Point(246, 0)
        Me.RectangleShape2.Name = "RectangleShape2"
        Me.RectangleShape2.Size = New System.Drawing.Size(24, 405)
        '
        'RectangleShape1
        '
        Me.RectangleShape1.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.RectangleShape1.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape1.FillGradientColor = System.Drawing.Color.Maroon
        Me.RectangleShape1.FillGradientStyle = Microsoft.VisualBasic.PowerPacks.FillGradientStyle.ForwardDiagonal
        Me.RectangleShape1.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.DiagonalCross
        Me.RectangleShape1.Location = New System.Drawing.Point(20, 0)
        Me.RectangleShape1.Name = "RectangleShape1"
        Me.RectangleShape1.Size = New System.Drawing.Size(24, 404)
        '
        'GamePanel
        '
        Me.GamePanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(221, Byte), Integer), CType(CType(168, Byte), Integer))
        Me.GamePanel.Location = New System.Drawing.Point(45, 0)
        Me.GamePanel.Name = "GamePanel"
        Me.GamePanel.Size = New System.Drawing.Size(201, 401)
        Me.GamePanel.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label1.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.Label1.Location = New System.Drawing.Point(271, 17)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(8, 0, 10, 0)
        Me.Label1.Size = New System.Drawing.Size(147, 27)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "HIGHSCORE"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label2.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(99, Byte), Integer), CType(CType(24, Byte), Integer), CType(CType(27, Byte), Integer))
        Me.Label2.Location = New System.Drawing.Point(271, 93)
        Me.Label2.Name = "Label2"
        Me.Label2.Padding = New System.Windows.Forms.Padding(35, 0, 35, 0)
        Me.Label2.Size = New System.Drawing.Size(146, 27)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "SCORE"
        '
        'highscore_lbl
        '
        Me.highscore_lbl.AutoSize = True
        Me.highscore_lbl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.highscore_lbl.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.highscore_lbl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(225, Byte), Integer), CType(CType(9, Byte), Integer), CType(CType(14, Byte), Integer))
        Me.highscore_lbl.Location = New System.Drawing.Point(283, 53)
        Me.highscore_lbl.Name = "highscore_lbl"
        Me.highscore_lbl.Size = New System.Drawing.Size(120, 27)
        Me.highscore_lbl.TabIndex = 2
        Me.highscore_lbl.Text = "000000000"
        '
        'score_lbl
        '
        Me.score_lbl.AutoSize = True
        Me.score_lbl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.score_lbl.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.score_lbl.ForeColor = System.Drawing.Color.MidnightBlue
        Me.score_lbl.Location = New System.Drawing.Point(283, 131)
        Me.score_lbl.Name = "score_lbl"
        Me.score_lbl.Size = New System.Drawing.Size(120, 27)
        Me.score_lbl.TabIndex = 2
        Me.score_lbl.Text = "000000000"
        '
        'PreviewPanel
        '
        Me.PreviewPanel.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.PreviewPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PreviewPanel.Location = New System.Drawing.Point(303, 282)
        Me.PreviewPanel.Name = "PreviewPanel"
        Me.PreviewPanel.Size = New System.Drawing.Size(83, 83)
        Me.PreviewPanel.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label5.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label5.Location = New System.Drawing.Point(281, 182)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 27)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Lines:  "
        '
        'lines_lbl
        '
        Me.lines_lbl.AutoSize = True
        Me.lines_lbl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.lines_lbl.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lines_lbl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lines_lbl.Location = New System.Drawing.Point(355, 182)
        Me.lines_lbl.Name = "lines_lbl"
        Me.lines_lbl.Size = New System.Drawing.Size(48, 27)
        Me.lines_lbl.TabIndex = 2
        Me.lines_lbl.Text = "000"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.Label7.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label7.Location = New System.Drawing.Point(281, 226)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 27)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "level: "
        '
        'level_lbl
        '
        Me.level_lbl.AutoSize = True
        Me.level_lbl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.level_lbl.Font = New System.Drawing.Font("Showcard Gothic", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.level_lbl.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.level_lbl.Location = New System.Drawing.Point(356, 226)
        Me.level_lbl.Name = "level_lbl"
        Me.level_lbl.Padding = New System.Windows.Forms.Padding(3, 0, 8, 0)
        Me.level_lbl.Size = New System.Drawing.Size(47, 27)
        Me.level_lbl.TabIndex = 2
        Me.level_lbl.Text = "00"
        '
        'PressToStart_lbl
        '
        Me.PressToStart_lbl.AutoSize = True
        Me.PressToStart_lbl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.PressToStart_lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PressToStart_lbl.Font = New System.Drawing.Font("Showcard Gothic", 26.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PressToStart_lbl.ForeColor = System.Drawing.Color.Crimson
        Me.PressToStart_lbl.Location = New System.Drawing.Point(70, 142)
        Me.PressToStart_lbl.Name = "PressToStart_lbl"
        Me.PressToStart_lbl.Padding = New System.Windows.Forms.Padding(8, 0, 10, 0)
        Me.PressToStart_lbl.Size = New System.Drawing.Size(302, 90)
        Me.PressToStart_lbl.TabIndex = 2
        Me.PressToStart_lbl.Text = "PRESS ANY KEY " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "TO START"
        Me.PressToStart_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlashStart
        '
        Me.FlashStart.Interval = 700
        '
        'BlockTimer
        '
        Me.BlockTimer.Interval = 350
        '
        'Leveler
        '
        Me.Leveler.Interval = 20000
        '
        'BTetris
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(77, Byte), Integer), CType(CType(33, Byte), Integer), CType(CType(124, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(415, 402)
        Me.Controls.Add(Me.PressToStart_lbl)
        Me.Controls.Add(Me.level_lbl)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lines_lbl)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.PreviewPanel)
        Me.Controls.Add(Me.score_lbl)
        Me.Controls.Add(Me.highscore_lbl)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GamePanel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "BTetris"
        Me.Text = "VB_Tetris - Lee Zhen Yong - 1122702848"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape1 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents RectangleShape2 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents GamePanel As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents highscore_lbl As System.Windows.Forms.Label
    Friend WithEvents score_lbl As System.Windows.Forms.Label
    Friend WithEvents PreviewPanel As System.Windows.Forms.Panel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lines_lbl As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents level_lbl As System.Windows.Forms.Label
    Friend WithEvents PressToStart_lbl As System.Windows.Forms.Label
    Friend WithEvents FlashStart As System.Windows.Forms.Timer
    Friend WithEvents BlockTimer As System.Windows.Forms.Timer
    Friend WithEvents Leveler As System.Windows.Forms.Timer
End Class
