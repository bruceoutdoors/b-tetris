﻿Imports B_Tetris_v1._0.TetrisBlock

Public Class BTetris
    ' audio
    Private gameMusic As New Audio("sound\Original Tetris theme (Tetris Soundtrack).mp3")
    Private looseGame As New Audio("sound\GameEnd.mid")
    Private moveSound As New Audio("sound\Move.mp3")
    Private rotateSound As New Audio("sound\Rotate.mp3")
    Private groundSound As New Audio("sound\Grounded.mp3")
    Private menuMusic As New Audio("sound\MainMenu.mid")
    Private congratsSong As New Audio("sound\NameEntry.mid")

    Private gameBoard, previewBoard As TetrisBoard
    Private fallingBlock, previewBlock As TetrisBlock

    Private lines, level, speed, score As Integer
    Private isStart As Boolean = False

    Private Sub BTetris_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        gameBoard = New TetrisBoard(GamePanel, 20, 10, 20)
        previewBoard = New TetrisBoard(PreviewPanel, 4, 4, 20)
        previewBlock = New TetrisBlock(previewBoard, Shapes.genRand, 2, 2)

        gameMusic.setVolume(200)
        highscore_lbl.Text = My.Settings.HighestScore.ToString("000000000")
        menuMusic.Play()
        FlashStart.Start()
    End Sub

    Private Sub gameStart()
        lines = 0
        level = 0
        score = 0
        speed = 300

        score_lbl.Text = score.ToString("000000000")
        level_lbl.Text = level.ToString("00")
        lines_lbl.Text = lines.ToString("000")

        isStart = True
        fallingBlock = New TetrisBlock(gameBoard, previewBlock.CurrentData, 2, gameBoard.Columns \ 2)
        previewBlock.setBlockShape(Shapes.genRand)

        FlashStart.Stop()
        menuMusic.Stop()
        PressToStart_lbl.Visible = False
        Leveler.Start()
        BlockTimer.Start()

        gameMusic.Loop()
    End Sub

    ' handles keyboard input
    Protected Overrides Function ProcessCmdKey(ByRef msg As Message, ByVal keyData As Keys) As Boolean
        Dim bHandled As Boolean = False
        With fallingBlock
            If isStart Then
                Select Case keyData
                    Case Keys.Left
                        If .CanMove(MoveDirection.LEFT) Then .Move(MoveDirection.LEFT)
                        moveSound.Play()
                        bHandled = True
                    Case Keys.Right
                        If .CanMove(MoveDirection.RIGHT) Then .Move(MoveDirection.RIGHT)
                        moveSound.Play()
                        bHandled = True
                    Case Keys.Down
                        If .CanMove(MoveDirection.DOWN) Then .Move(MoveDirection.DOWN)
                        moveSound.Play()
                        bHandled = True
                    Case Keys.Up
                        If .CanRotate Then .Rotate()
                        rotateSound.Play()
                        bHandled = True
                    Case Keys.Space
                        .Crash()
                End Select
            Else
                gameStart()
            End If
        End With
        Return bHandled
    End Function

    Private Sub FlashStart_Tick(sender As System.Object, e As System.EventArgs) Handles FlashStart.Tick
        If PressToStart_lbl.Visible Then
            PressToStart_lbl.Visible = False
        Else
            PressToStart_lbl.Visible = True
        End If
    End Sub

    Private Sub BlockTimer_Tick(sender As System.Object, e As System.EventArgs) Handles BlockTimer.Tick
        If fallingBlock.CanMove(MoveDirection.DOWN) Then
            fallingBlock.Move(MoveDirection.DOWN)
            Exit Sub
        End If
        groundSound.Play()
        fallingBlock.IsGrounded = True
        Dim checkRows = From cell In fallingBlock.BlockCells Order By cell.Row Select cell.Row Distinct
        Dim rowsRemoved As Integer = 0
        For Each row In checkRows
            If gameBoard.IsRowComplete(row) Then
                Dim fallingCluster As New CellCluster(gameBoard, gameBoard.justRemoveRow(row))
                fallingCluster.Drop(speed)
                rowsRemoved += 1
            End If
        Next

        Select Case rowsRemoved
            Case 1
                score += 40 * (level + 1)
            Case 2
                score += 100 * (level + 1)
            Case 3
                score += 300 * (level + 1)
            Case 4
                score += 1200 * (level + 1)
        End Select
        score_lbl.Text = score.ToString("000000000")
        lines += rowsRemoved
        lines_lbl.Text = lines.ToString("000")
        BlockTimer.Stop()
        DropNextFallingBlock()
    End Sub

    Private Sub DropNextFallingBlock()
        fallingBlock = New TetrisBlock(gameBoard, previewBlock.CurrentData, 2, gameBoard.Columns \ 2)

        ' check if game has ended
        If Not fallingBlock.CanMove(MoveDirection.DOWN) Then
            GameEnd()
            Exit Sub
        End If

        BlockTimer.Interval = speed
        BlockTimer.Start()
        previewBlock.setBlockShape(Shapes.genRand)
    End Sub

    Private Sub GameEnd()
        gameMusic.Stop()
        Leveler.Stop()

        If score < My.Settings.HighestScore Then
            looseGame.Play()
            MessageBox.Show("You have lost.")
        Else
            congratsSong.Play()
            My.Settings.HighestScore = score
            highscore_lbl.Text = My.Settings.HighestScore.ToString("000000000")
            MessageBox.Show("CONGRATULATIONS!! " & vbCrLf & "You have topped the highest score!!")
        End If

        looseGame.Stop()
        congratsSong.Stop()

        menuMusic.Play()
        isStart = False
        gameBoard.SetupBoard()
        FlashStart.Start()
    End Sub

    Private Sub Leveler_Tick(sender As System.Object, e As System.EventArgs) Handles Leveler.Tick
        If level <> 99 Then
            level += 1
            level_lbl.Text = level.ToString("00")
        End If
        If speed <> 100 Then speed -= 25
    End Sub

End Class