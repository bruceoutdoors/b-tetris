﻿Public Class CellCluster
    Private parentBoard As TetrisBoard

    Public gravity_timer As New System.Windows.Forms.Timer
    ' represents the cells on this block
    Public BlockCells() As TetrisCell

    Public Sub New(ByVal parentBoard As TetrisBoard, ByVal cells() As TetrisCell)
        Me.parentBoard = parentBoard
        AddHandler gravity_timer.Tick, AddressOf makeDrop
        BlockCells = cells
        IsGrounded = False
    End Sub

    Public Property IsGrounded() As Boolean
        Get
            Return _isGrounded
        End Get
        Set(value As Boolean)
            _isGrounded = value
            If _isGrounded Then
                For Each cell In BlockCells
                    cell.IsGrounded = True
                Next
            Else
                For Each cell In BlockCells
                    cell.IsGrounded = False
                Next
            End If
        End Set
    End Property

    Public Sub Move()
        [Erase]()
        ' shift chunk downwards
        Dim i As Integer = 0
        For Each cell In BlockCells
            ' we need the block to retain it's color after transition by using this temp variable
            Dim tempColor As Color = BlockCells(i).HighlightColor
            ' ...because the cell we're moving to may contains a different highlight color
            BlockCells(i) = parentBoard.Cells(cell.Row + 1, cell.Column)
            BlockCells(i).HighlightColor = tempColor
            i += 1
        Next
        Draw()
    End Sub

    Public Function CanMove() As Boolean
        For Each cell In BlockCells
            cell = parentBoard.Cells(cell.Row + 1, cell.Column)
            If cell Is Nothing OrElse cell.IsGrounded Then Return False
        Next
        Return True
    End Function

    ' puts gravity into tetris block
    Public Sub Drop(ByVal interval As Integer)
        gravity_timer.Interval = interval
        gravity_timer.Start()
    End Sub

    Private _isGrounded As Boolean = False

    ' handles the gravity timer
    Private Sub makeDrop()
        If CanMove() Then
            Move()
        Else
            ' Debug.WriteLine("touch down!")
            gravity_timer.Stop()
            IsGrounded = True
        End If
    End Sub

    ' colors and highlights the block
    Private Sub Draw()
        For Each cell In BlockCells
            cell.Highlighted = True
        Next
    End Sub

    ' erases the block 
    Private Sub [Erase]()
        For Each cell In BlockCells
            cell.Highlighted = False
        Next
    End Sub
End Class
