﻿Public Class TetrisBoard
    Private parent As Control

    Public Rows, Columns, CellSize As Integer

    'places this class inside a container defined by constructor
    '(in this case it's a panel)
    Public Sub New(ByVal parent As Control, ByVal rows As Integer, _
                   ByVal columns As Integer, ByVal cellSize As Integer)
        Me.parent = parent
        Me.Rows = rows
        Me.Columns = columns
        Me.CellSize = cellSize
        SetupBoard()
    End Sub

    ' this function helps CanMove method in TetrisBlock to ensure all blocks stay in the board
    ' returns Nothing if it exceed boundaries, return the tetris cell that takes up this row and column
    ' if it within bounds

    Public ReadOnly Property Cells(ByVal row As Integer, ByVal column As Integer) As TetrisCell
        Get
            'make sure the blocks does not go out of the boundaries of the tetris board 2D array:
            If row < 0 OrElse column < 0 OrElse row > Rows OrElse column > Columns Then Return Nothing
            Return _Cells(row, column)
        End Get
    End Property

    ' setup/reset board
    Public Sub SetupBoard()
        Me.parent.Controls.Clear()
        canvas.Shapes.Clear()
        Me.parent.Controls.Add(canvas)
        ReDim _Cells(Rows, Columns)
        For row = 1 To Rows
            For col = 1 To Columns
                _Cells(row, col) = New TetrisCell
                With Cells(row, col)
                    .Row = row
                    .Column = col
                    ' set parent of each cell to shape container, 
                    ' effectively adding it inside the container as well
                    .Parent = canvas
                    .Width = CellSize
                    .Height = CellSize
                    .Location = New Point((col - 1) * CellSize, (row - 1) * CellSize)
                End With
            Next
        Next
    End Sub

    ' client inputs a row number and IsRowComplete will output False if it finds any grounded TetrisCell in that row
    Public Function IsRowComplete(ByVal row As Integer) As Boolean
        For column As Integer = 1 To Columns
            If Not Cells(row, column).IsGrounded Then Return False
        Next
        Return True
    End Function

    Public Sub RemoveRow(ByVal row As Integer)
        ' boolean used to determine if row is empty,  if so it means we have reached the 'peak'
        ' therefore we'll tell the program to stop copying cells otherwise it will also shift cells
        ' mark as background, which would be a waste of processing power
        Dim peakFound As Boolean
        ' to remove one row, you must shift all the TetrisCell above that row down 1 row
        ' this loop iterates all the rows above the row that needs to be removed
        For selectedRow As Integer = row - 1 To 0 Step -1
            ' set initial value of peakFound. Given that any of the cells the next iterates is grounded,
            ' this value will be set to False, which indicates that this row still has cells to be move
            ' and that we have not reached the peak
            peakFound = True
            ' within the above loop, this loop iterates all the individual TetrisCell in every selected row
            For column As Integer = 1 To Columns
                ' for every TetrisCell in this row, copy HighlightColor, Highlighted, IsEmpty properties
                ' to the cell below it
                Dim cellToMove As TetrisCell = Cells(selectedRow, column)
                Dim cellBeneath As TetrisCell = Cells(selectedRow + 1, column)
                cellBeneath.HighlightColor = cellToMove.HighlightColor
                ' take note that changing this property modifies both highlight color and border style
                cellBeneath.Highlighted = cellToMove.Highlighted
                cellBeneath.IsGrounded = cellToMove.IsGrounded
                ' if cell is grounded, peak is not found yet
                If cellToMove.IsGrounded Then peakFound = False
            Next
            ' if peak is found the loop terminates without needing to iterate rows of empty cells
            If peakFound Then Exit For
        Next
    End Sub

    Public Function justRemoveRow(ByVal row As Integer)
        For column As Integer = 1 To Columns
            Dim cellToRemove As TetrisCell = Cells(row, column)
            cellToRemove.Highlighted = False
            cellToRemove.IsGrounded = False
        Next

        Dim tetrisCollection As New List(Of TetrisCell)

        Dim peakFound As Boolean
        For selectedRow As Integer = row - 1 To 0 Step -1
            peakFound = True
            For column As Integer = 1 To Columns
                Dim cellToStore As TetrisCell = Cells(selectedRow, column)
                If cellToStore IsNot Nothing And cellToStore.IsGrounded Then
                    tetrisCollection.Add(cellToStore)
                End If
                If cellToStore.IsGrounded Then peakFound = False
            Next
            If peakFound Then Exit For
        Next
        Dim returnedCells(tetrisCollection.Count - 1) As TetrisCell
        For i As Integer = 0 To tetrisCollection.Count - 1
            returnedCells(i) = tetrisCollection(i)
            'Debug.WriteLine(returnedCells(i).Row & ", " & returnedCells(i).Column)
        Next
        Return returnedCells
    End Function

    Private canvas As New Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Private _Cells(,) As TetrisCell

End Class
