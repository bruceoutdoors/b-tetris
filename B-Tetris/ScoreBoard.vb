﻿Public Class ScoreBoard
    Private congratsSong As New Audio("sound\NameEntry.mid")

    Private Sub ScoreBoard_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        congratsSong.Play()

        'create table on first run
        If My.Settings.ScoreTable Is Nothing Then
            ' initialize your table to make it available for use
            My.Settings.ScoreTable = New DataTable
            ' set data table any name just so it can save your data
            My.Settings.ScoreTable.TableName = "BULLSHIT"

            ' Create columns:
            With My.Settings.ScoreTable.Columns
                .Add("# ", GetType(Integer))
                .Add("NAME", GetType(String))
                .Add("SCORE", GetType(UInteger))
                .Add("DATE", GetType(DateTime))
            End With

            ' initial row values:
            With My.Settings.ScoreTable.Rows
                .Add(1, "Beefy Jack", 809210, DateTime.Now)
                .Add(2, "Morphic Morpheus", 506360, DateTime.Now)
                .Add(3, "Eye Dandy", 89880, DateTime.Now)
                .Add(4, "Monty Python", 12230, DateTime.Now)
                .Add(5, "Lightnin' Dimmer", 5590, DateTime.Now)
            End With
        End If

        ' set columns
        For Each column In My.Settings.ScoreTable.Columns
            HighScoreBoard.Columns.Add(column.ToString)
        Next

        'set column widths and alignment
        With HighScoreBoard.Columns
            .Item(0).Width = 20
            .Item(1).Width = 110
            .Item(2).Width = 60
            .Item(2).TextAlign = HorizontalAlignment.Right
            .Item(3).Width = 130
        End With

        'display row contents
        Display_Data()
    End Sub

    Private Sub Display_Data()
        ' clear listView1 if it has items
        HighScoreBoard.Items.Clear()

        ' fill rows with data
        For i = 0 To My.Settings.ScoreTable.Rows.Count - 1
            'Dim rowItem As New ListViewItem(table.Rows.Item(i).Item(0).ToString)
            Dim rowItem(3) As String
            Dim j As Integer = 0
            For Each row In My.Settings.ScoreTable.Rows.Item(i).ItemArray
                'rowItem.SubItems.Add(table.Rows.Item(i).Item(j))
                rowItem(j) = row
                j += 1
            Next
            Dim item As New ListViewItem(rowItem)
            HighScoreBoard.Items.Add(item)
        Next
    End Sub

    Private Sub play_btn_Click(sender As System.Object, e As System.EventArgs) Handles play_btn.Click
        Me.Hide()
        congratsSong.Stop()
        BTetris.Show()
    End Sub

    ' on close
    Private Sub ScoreBoard_Close(sender As System.Object, e As System.EventArgs) Handles MyBase.FormClosed
        congratsSong.Stop()
        BTetris.Show()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        My.Settings.Reset()
    End Sub
End Class
