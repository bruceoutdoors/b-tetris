﻿Public Class TetrisBlock
    Private parentBoard As TetrisBoard

    Public BlockCells() As TetrisCell
    Public CurrentData As Shapes.blockData

    Public Enum MoveDirection
        LEFT
        RIGHT
        DOWN
        UP
    End Enum

    ' with every new block you must define it's shape and position
    Public Sub New(ByVal parentBoard As TetrisBoard, ByVal InitShape As Shapes.blockData, _
                   ByVal row As Integer, ByVal column As Integer)
        ' set parent
        Me.parentBoard = parentBoard
        ' set block properties
        CurrentData = InitShape
        ' set initial position of center cell
        centerCell = parentBoard.Cells(row, column)
        ' then update the block based on new position of center cell
        BlockCells = GetCells(centerCell, CurrentData.Shape(rotationIndex))
        Draw()
    End Sub

    Public Property IsGrounded() As Boolean
        Get
            Return _isGrounded
        End Get
        Set(value As Boolean)
            _isGrounded = value
            If _isGrounded Then
                For Each cell In BlockCells
                    cell.IsGrounded = True
                Next
            Else
                For Each cell In BlockCells
                    cell.IsGrounded = False
                Next
            End If
        End Set
    End Property

    ' colors and highlights the block
    Public Sub Draw()
        For Each cell In BlockCells
            cell.HighlightColor = CurrentData.blockColor
            cell.Highlighted = True
        Next
    End Sub

    ' erases the block 
    Public Sub [Erase]()
        For Each cell As TetrisCell In BlockCells
            cell.Highlighted = False
        Next
    End Sub

    Public Sub Rotate()
        [Erase]()
        ' change rotation/shape
        If rotationIndex = CurrentData.Shape.Count - 1 Then
            rotationIndex = 0
        Else
            rotationIndex += 1
        End If
        BlockCells = GetCells(centerCell, CurrentData.Shape(rotationIndex))
        Draw()
    End Sub

    Public Sub Move(ByVal direction As MoveDirection)
        [Erase]()
        ' change the center cell to the target position
        Select Case direction
            Case MoveDirection.LEFT
                centerCell = parentBoard.Cells(centerCell.Row, centerCell.Column - 1)
            Case MoveDirection.RIGHT
                centerCell = parentBoard.Cells(centerCell.Row, centerCell.Column + 1)
            Case MoveDirection.DOWN
                centerCell = parentBoard.Cells(centerCell.Row + 1, centerCell.Column)
            Case MoveDirection.UP
                centerCell = parentBoard.Cells(centerCell.Row - 1, centerCell.Column)
        End Select
        BlockCells = GetCells(centerCell, CurrentData.Shape(rotationIndex))
        Draw()
    End Sub

    Public Function CanMove(ByVal direction As MoveDirection) As Boolean
        Dim tempCenterCell As New TetrisCell
        ' now move that cell to the possible location
        Select Case direction
            Case MoveDirection.LEFT
                tempCenterCell = parentBoard.Cells(centerCell.Row, centerCell.Column - 1)
            Case MoveDirection.RIGHT
                tempCenterCell = parentBoard.Cells(centerCell.Row, centerCell.Column + 1)
            Case MoveDirection.DOWN
                tempCenterCell = parentBoard.Cells(centerCell.Row + 1, centerCell.Column)
            Case MoveDirection.UP
                tempCenterCell = parentBoard.Cells(centerCell.Row - 1, centerCell.Column)
        End Select
        ' If cell is out of bounds it means it can't move, therefore returns false
        If tempCenterCell Is Nothing Then Return False

        ' create dummy block to test if any of the cells in that block cross boundaries
        Dim dummyBlock As TetrisCell() = GetCells(tempCenterCell, CurrentData.Shape(rotationIndex))
        For Each cell In dummyBlock
            If cell Is Nothing OrElse cell.IsGrounded Then Return False
        Next
        Return True
    End Function

    Public Function CanRotate() As Boolean
        ' if there's only one shape, then rotating it wastes processing power
        ' so just say it's not rotatable
        If CurrentData.Shape.Count = 1 Then
            Return False
        End If
        ' create test index
        Dim tempIndex As Integer

        ' let temporary index be the possible index
        If rotationIndex = CurrentData.Shape.Count - 1 Then
            tempIndex = 0
        Else
            tempIndex = rotationIndex + 1
        End If

        ' now create dummy block to test if by using this index it will cross bounds
        Dim dummyBlock As TetrisCell() = GetCells(centerCell, CurrentData.Shape(tempIndex))
        For Each cell In dummyBlock
            If cell Is Nothing OrElse cell.IsGrounded Then Return False
        Next
        Return True
    End Function

    Public Sub setBlockPosition(ByVal row As Integer, ByVal column As Integer)
        [Erase]()
        ' change center cell
        centerCell = parentBoard.Cells(row, column)
        ' then update the block based on new position of center cell
        BlockCells = GetCells(centerCell, CurrentData.Shape(rotationIndex))
        Draw()
    End Sub

    Public Sub setBlockColor(ByVal thisColor As Color)
        CurrentData.blockColor = thisColor
        For Each cell In BlockCells
            cell.HighlightColor = CurrentData.blockColor
        Next
    End Sub

    Public Sub setBlockShape(ByVal thisShape As Shapes.blockData)
        [Erase]()
        CurrentData = thisShape
        ' reset rotation index, because different blocks have different rotations
        rotationIndex = 0
        BlockCells = GetCells(centerCell, CurrentData.Shape(rotationIndex))
        Draw()
    End Sub

    ' drops the block instantly to the ground
    Public Sub Crash()
        While CanMove(MoveDirection.DOWN)
            Move(MoveDirection.DOWN)
        End While
    End Sub

    Private rotationIndex As Integer = 0
    ' the cell where movement and rotation is measured from
    Private centerCell As TetrisCell
    Private _isGrounded As Boolean

    ' returns an array of class TetrisCell which represents a tetris block
    Private Function GetCells(ByVal center As TetrisCell, ByVal ParamArray relativeCoordinates() As Integer) As TetrisCell()
        ' given every 2 integers is 1 coordinate, and array starts with 0:
        ' given that all tetris blocks take up 4 cells, the size will always be 3, counting 0 in
        Dim theCells((relativeCoordinates.Count \ 2) - 1) As TetrisCell
        Dim refRow As Integer = center.Row
        Dim refCol As Integer = center.Column

        ' Count property returns the size of the array, where theCells(3) means from 0 -> 3 so it's 4
        For i As Integer = 0 To theCells.Count - 1
            ' processes an array in pairs, calculates the coordinates of the required tetris cell stores in theCells()
            theCells(i) = parentBoard.Cells(refRow + relativeCoordinates(i * 2), refCol + relativeCoordinates(i * 2 + 1))
        Next
        Return theCells
    End Function
End Class
