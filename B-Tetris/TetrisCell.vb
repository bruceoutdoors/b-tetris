﻿Public Class TetrisCell
    Inherits Microsoft.VisualBasic.PowerPacks.RectangleShape

    Property Row As Integer
    Property Column As Integer
    Property IsGrounded As Boolean = False

    Public Sub New()
        Me.BackStyle = PowerPacks.BackStyle.Opaque
        Me.BorderStyle = Drawing2D.DashStyle.Custom
        Me.BorderColor = Color.Black
        ' so user does not select the cells
        Me.Enabled = False
    End Sub

    Public Property Highlighted() As Boolean
        Get
            Return _Highlighted
        End Get
        Set(ByVal value As Boolean)
            _Highlighted = value
            ' if true then that means it's not empty so set the color 
            If _Highlighted Then
                Me.BackColor = HighlightColor
                Me.BorderStyle = Drawing2D.DashStyle.Solid
            Else
                ' set the cell to blend into the background
                Me.BackColor = Me.Parent.BackColor
                Me.BorderStyle = Drawing2D.DashStyle.Custom
            End If
        End Set
    End Property

    Public Property HighlightColor() As Color
        Get
            Return _HighlightColor
        End Get
        Set(ByVal value As Color)
            _HighlightColor = value
            If Me.Highlighted Then Me.BackColor = _HighlightColor
        End Set
    End Property

    Private _Highlighted As Boolean = False
    Private _HighlightColor As Color

End Class
